\documentclass[absolute, overlay]{TIBbeamer}

%%%% Usepackages

\usepackage[english]{babel}
\usepackage{color}
\usepackage{epstopdf}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{xcolor}

%%%% Miscellaneous Settings

\graphicspath{{graphics/}}

\defbeamertemplate{description item}{align left}{\insertdescriptionitem\hfill}

\AtBeginSection{\frame{\sectionpage}}

%%%% Title Page

\title{Specialised information infrastructure services for applied mathematicians and maths-related researchers}

\author{Mila Runnwerth}

\date{Joint Annual Meeting of GAMM and DMV, March 8, 2016}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Table of Contents}
\thispagestyle{empty}
\tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Mathematics Information Service}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Mathematics Information Service}

\centering
\includegraphics[width=0.9\linewidth]{fid}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{TIB's Expertise}

\begin{figure}
\begin{minipage}{0.55\linewidth}
\flushleft
\begin{itemize}
\item German National Library of Science \& Technology.
\item Access to more than five million items of mathematical information.
\item Research foci: Visual Analytics, Data Science, Open Science, Non-textual Materials.
\end{itemize}
\end{minipage}
\begin{minipage}{0.4\linewidth}
\begin{center}
\includegraphics[width=0.9\linewidth]{luh}
\end{center}
\centering
\tiny{By courtesy of Michael Hohlfeld.}
\end{minipage}
\end{figure}

\href{https://www.tib.eu/en/}{Visit us here.}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The Research Process}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The Research Process in General and Some Services}

\begin{center}
\includegraphics[width=0.75\linewidth]{CoScienceCircle_TIBServices}
\end{center}
\centering
\tiny{\href{http://handbuch.io/w/Handbuch\_CoScience}{http://handbuch.io/w/Handbuch\_CoScience}}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The (Applied) Mathematician's Research Process}

\begin{center}
\includegraphics[width=0.75\linewidth]{researchprocess}
\end{center}
\centering
\tiny{\href{http://www.nottingham.ac.uk/csme/meas/papers/yasukawa.gif}{K. Yasukawa: Looking at mathematics as technology: implications for numeracy}}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{The (Applied) Mathematician's Tools}

\centering
\includegraphics[width=0.7\linewidth]{tools}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Software}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{``Better Software -- Better Research''}

\begin{figure}
\begin{minipage}{0.7\linewidth}
\flushleft
The British Software Sustainability Institute proclaims an improved awareness of software in science.

\begin{itemize}
\item 92\% of academics use research software;
\item 69\% say that their research would not be practical without it;
\item 56\% develop their own software (21\% of those have no training in software development)
\item 70\% of male researchers develop their own software, and only 30\% of female researchers do so
\end{itemize}

\href{http://www.software.ac.uk/blog/2014-12-04-its-impossible-conduct-research-without-software-say-7-out-10-uk-researchers}{Here you find the survey.}
\end{minipage}
\begin{minipage}{0.25\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{BSSI}
\end{minipage}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Software in the mathematical sciences}

Applications in (applied) mathematics are:

\begin{itemize}
\item Mathematical modelling.
\item Computational simulation.
\end{itemize}

They typically occur as:

\begin{itemize}
\item Scripts.
\item Software systems.
\item Platforms.
\item Web Services
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{User perspective}

\begin{itemize}
\item How do I find relevant software and its documentation?
\item Which software is appropriately accessible?
\item Are parameter sets available to reproduce simulations?
\item \alert{How do I cite / publish software?} (Main objective of the Mathematics Information Service -- Maths Beyond Text.)
\item How do I archive software properly (long-term availability, legal issues, \ldots)?
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Outlook to Services}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{User expectation}

\begin{figure}
\begin{minipage}{0.5\linewidth}
\flushleft
\begin{itemize}[<+->]
\item Easy workflow to upload and archive data.
\item Easy access (search, documentation, download).
\item Distinguish modelling, simulation software or measuring data or little helpers.
\item Easy referencing.
\item Easy and transperant data policy.
\item Easy copyright management.
\end{itemize}
\end{minipage}
\begin{minipage}{0.45\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{repository}
\end{minipage}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{FAIRDOM}

\begin{figure}
\begin{minipage}{0.6\linewidth}
\flushleft
\begin{itemize}
\item \alert{F}indable
\item \alert{A}ccessible
\item \alert{I}nteroperable
\item \alert{R}eusable
\end{itemize}

\vspace{\baselineskip}

\href{http://fair-dom.org/}{Link to the FAIRDOM initiative.}
\end{minipage}
\begin{minipage}{0.35\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{fairdom}
\end{minipage}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{State of the Art Services}

\centering
\includegraphics[width=0.9\linewidth]{stateoftheart}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Outlook}

\begin{itemize}[<+->]
\item There are many classes of software which demand different consideration concerning citation and publication.
\item Software as a topic in information competence.
\item Do not miss Helge Holzmann's talk \emph{A Vision on Software Citations} in tomorrow's session.
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Contact, Discussion, Questions}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Contact, Discussion, Questions}

\begin{figure}
\begin{minipage}{0.4\linewidth}
Mila Runnwerth \\
Tel.: +49 511 762 3979 \\
\href{mailto:mila.runnwerth@tib.eu}{mila.runnwerth@tib.eu}

\vspace{\baselineskip}

\href{https://bitbucket.org/runnwerth/gammdmv2016/src}{Here you find the slides.}
\end{minipage}
\begin{minipage}{0.5\linewidth}
\centering
\includegraphics[width=0.9\linewidth]{questions} \\
\tiny{\href{https://xkcd.com/1256/}{https://xkcd.com/1256/}}
\end{minipage}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}\grid
